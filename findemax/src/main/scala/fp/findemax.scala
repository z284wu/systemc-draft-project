
package fp

import chisel3._
import chisel3.util.Decoupled

class FP(private val E: Int, private val F: Int) extends Bundle {
  val sign = FP.sign_t
  val expo = FP.expo_t(E)
  val frac = FP.frac_t(F)
}
object FP {
  def apply(E: Int, F: Int): FP = new FP(E, F)
  def apply(ui: Int, E: Int, F: Int): FP = ui.U.asTypeOf(FP(E, F))

  def frac_t(F: Int) = UInt(F.W)
  def expo_t(E: Int) = UInt(E.W)
  def expo_t(value: Int, E: Int) = 0.U(E.W)
  def sign_t = UInt(1.W)
}
class SRegFWREV[T <: Data](val genT: T, val defaultT: T) extends Module {
  val io = IO(new Bundle {
    val s_port = Flipped(Decoupled(genT))
    val m_port = Decoupled(genT)
  })
  val IW = 1
  val RLEV = false
  val depth = 2

  val data = RegInit(VecInit(Seq.fill(depth) { defaultT }))

  val rd_idx = RegInit(0.U(IW.W))
  val wr_idx = RegInit(0.U(IW.W))

  val wr_en_i = Wire(Bool())
  val rd_en_i = Wire(Bool())
  val full_i  = RegInit(false.B)
  val empty_i = RegInit(true.B)

  mc_proc()
  ms_proc()

  def mc_proc(): Unit = {
    io.m_port.bits := data(rd_idx)

    wr_en_i := io.s_port.valid && !full_i
    rd_en_i := io.m_port.ready && !empty_i

    io.s_port.ready := !full_i
    io.m_port.valid := !empty_i
  }
  def ms_proc(): Unit = {
    val wr_inc = WireInit((wr_idx + 1.U) % depth.U)
    val rd_inc = WireInit((rd_idx + 1.U) % depth.U)
      when (wr_en_i) {
        data(wr_idx) := io.s_port.bits
        wr_idx := wr_inc
        when (!rd_en_i) {
          when (wr_inc === rd_idx) {
            full_i := true.B
          }
          empty_i := false.B
        }
      }
      when (rd_en_i) {
        rd_idx := rd_inc
        when (!wr_en_i) {
          full_i := false.B
          when (rd_inc === wr_idx) { empty_i := true.B }
        }
      }

  }
}
// Note assume using positive edge
class SFIFOCC[T <: Data](val genT: T, val defaultT:() => T, val depth: Int)
  extends Module {
  private val IW:Int = 16
  val io = IO(new Bundle {
    val s_port = Flipped(Decoupled(genT))
    val m_port = Decoupled(genT)
  })

  val data = RegInit(VecInit(Seq.fill(depth) { defaultT() }))

  val rd_idx = RegInit(0.U(IW.W))
  val wr_idx = RegInit(0.U(IW.W))

  val wr_en_i = WireInit(false.B)
  val rd_en_i = WireInit(false.B)
  val full_i = RegInit(false.B)
  val empty_i = RegInit(true.B)

  mc_proc()
  ms_proc()

  def mc_proc(): Unit = {
    io.m_port.bits := data(rd_idx)

    wr_en_i := io.s_port.valid && !full_i
    rd_en_i := io.m_port.ready && !empty_i

    io.s_port.ready := !full_i
    io.m_port.valid := !empty_i
  }
  def ms_proc(): Unit = {
    val wr_inc = WireInit((wr_idx + 1.U) % depth.U)
    val rd_inc = WireInit((rd_idx + 1.U) % depth.U)
    when (wr_en_i) {
      data(wr_idx) := io.s_port.bits
      wr_idx := wr_inc
      when (!rd_en_i) {
        when (wr_inc === rd_idx) {
          full_i := true.B
        }
        empty_i := false.B
      }
    }
    when (rd_en_i) {
      rd_idx := rd_inc
      when (!wr_en_i) {
        full_i := false.B
        when (rd_inc === wr_idx) {
          empty_i := true.B
        }
      }
    }
  }
}
class Findemax(val E: Int, val F: Int, val DIM: Int) extends Module{
  val genFP = FP(E, F)
  val genFPDefault = FP(0, E, F)
  val genFPexpo = FP.expo_t(E)
  val genFPexpoDefault = FP.expo_t(0, E)
  val io = IO(new Bundle {
    val s_fp = Flipped(Decoupled(genFP))
    val m_fp = Decoupled(genFP)
    val m_ex = Decoupled(genFPexpo)
  })

  private def fpblk_sz(x: Int): Int = 1 << 2 * x

  /*-------- registers --------*/
  val count = RegInit(UInt((2*DIM).W), fpblk_sz(DIM).U)
  val emax = RegInit(FP.expo_t(E), 0.U)
  val emax_v = RegInit(false.B)


  /*-------- channels --------*/
  val c_sync = Wire(Bool())
  val c_fp = Wire(Decoupled(FP(E, F)))
  val c_ex = Wire(Decoupled(FP.expo_t(E)))

  /*-------- modules --------*/
  /*
  sfifo_cc<FP> u_que_fp;
  sreg<expo_t,FWD_REV> u_reg_ex;
   */
  val u_reg_ex = Module(new SRegFWREV(genFPexpo, genFPexpoDefault))
  val u_que_fp = Module(new SFIFOCC[FP](genFP, () => FP(0, E, F), fpblk_sz(DIM)))

  io.m_ex <> u_reg_ex.io.m_port
  u_reg_ex.io.s_port <> c_ex
  io.s_fp <> u_que_fp.io.s_port
  u_que_fp.io.s_port <> c_fp
  io.m_fp <> u_que_fp.io.m_port

  mc_proc()
  ms_proc()

  def mc_proc(): Unit = {
    io.s_fp.ready := c_sync

    c_fp.bits := io.s_fp.bits
    c_fp.valid := c_sync

    c_ex.valid := emax_v
    c_ex.bits := emax

    c_sync := io.s_fp.valid && /* u_que_fp.io.s_port.ready*/ c_fp.ready && (!emax_v || u_reg_ex.io.s_port.ready)
  }
  def ms_proc(): Unit = {
    // reset signal is implicit in RegInit
    val last = count === 0.U
    val fp = io.s_fp.bits
    when(c_sync) {
      when(last) { count := fpblk_sz(DIM).U }
      .otherwise { count := count - 1.U }
    }
    when(emax_v && u_reg_ex.io.s_port.ready) {
      when(io.s_fp.valid) { emax := fp.expo }.otherwise { emax := 0.U }
    }.elsewhen(io.s_fp.valid && fp.expo > emax) {
      emax := fp.expo
    }

    when(emax_v && u_reg_ex.io.s_port.ready) { emax_v := false.B }
    .elsewhen(c_sync && last) { emax_v := true.B }
  }

}

object findemaxGen extends App {
  chisel3.Driver.execute(args, () => new Findemax(11, 52, DIM=2))
}