module SRegFWREV(
  input         clock,
  input         reset,
  output        io_s_port_ready,
  input         io_s_port_valid,
  input  [10:0] io_s_port_bits,
  input         io_m_port_ready,
  output        io_m_port_valid,
  output [10:0] io_m_port_bits
);
  reg [10:0] data_0; // @[findemax.scala 30:21]
  reg [31:0] _RAND_0;
  reg [10:0] data_1; // @[findemax.scala 30:21]
  reg [31:0] _RAND_1;
  reg  rd_idx; // @[findemax.scala 32:23]
  reg [31:0] _RAND_2;
  reg  wr_idx; // @[findemax.scala 33:23]
  reg [31:0] _RAND_3;
  reg  full_i; // @[findemax.scala 37:24]
  reg [31:0] _RAND_4;
  reg  empty_i; // @[findemax.scala 38:24]
  reg [31:0] _RAND_5;
  wire  _T_1; // @[findemax.scala 46:35]
  wire  wr_en_i; // @[findemax.scala 46:32]
  wire  _T_3; // @[findemax.scala 47:35]
  wire  rd_en_i; // @[findemax.scala 47:32]
  wire  _T_8; // @[findemax.scala 53:35]
  wire [1:0] _GEN_18; // @[findemax.scala 53:42]
  wire [1:0] _GEN_0; // @[findemax.scala 53:42]
  wire  _T_9; // @[findemax.scala 53:42]
  wire  _T_12; // @[findemax.scala 54:35]
  wire [1:0] _GEN_19; // @[findemax.scala 54:42]
  wire [1:0] _GEN_1; // @[findemax.scala 54:42]
  wire  _T_13; // @[findemax.scala 54:42]
  wire  _T_15; // @[findemax.scala 58:15]
  wire  _T_16; // @[findemax.scala 59:24]
  wire  _GEN_4; // @[findemax.scala 59:36]
  wire  _GEN_6; // @[findemax.scala 58:25]
  wire  _GEN_11; // @[findemax.scala 55:22]
  wire  _T_17; // @[findemax.scala 67:15]
  wire  _T_18; // @[findemax.scala 69:24]
  wire  _GEN_12; // @[findemax.scala 69:36]
  wire  _GEN_14; // @[findemax.scala 67:25]
  wire  _GEN_17; // @[findemax.scala 65:22]
  assign _T_1 = full_i == 1'h0; // @[findemax.scala 46:35]
  assign wr_en_i = io_s_port_valid & _T_1; // @[findemax.scala 46:32]
  assign _T_3 = empty_i == 1'h0; // @[findemax.scala 47:35]
  assign rd_en_i = io_m_port_ready & _T_3; // @[findemax.scala 47:32]
  assign _T_8 = wr_idx + 1'h1; // @[findemax.scala 53:35]
  assign _GEN_18 = {{1'd0}, _T_8}; // @[findemax.scala 53:42]
  assign _GEN_0 = _GEN_18 % 2'h2; // @[findemax.scala 53:42]
  assign _T_9 = _GEN_0[0]; // @[findemax.scala 53:42]
  assign _T_12 = rd_idx + 1'h1; // @[findemax.scala 54:35]
  assign _GEN_19 = {{1'd0}, _T_12}; // @[findemax.scala 54:42]
  assign _GEN_1 = _GEN_19 % 2'h2; // @[findemax.scala 54:42]
  assign _T_13 = _GEN_1[0]; // @[findemax.scala 54:42]
  assign _T_15 = rd_en_i == 1'h0; // @[findemax.scala 58:15]
  assign _T_16 = _T_9 == rd_idx; // @[findemax.scala 59:24]
  assign _GEN_4 = _T_16 | full_i; // @[findemax.scala 59:36]
  assign _GEN_6 = _T_15 ? 1'h0 : empty_i; // @[findemax.scala 58:25]
  assign _GEN_11 = wr_en_i ? _GEN_6 : empty_i; // @[findemax.scala 55:22]
  assign _T_17 = wr_en_i == 1'h0; // @[findemax.scala 67:15]
  assign _T_18 = _T_13 == wr_idx; // @[findemax.scala 69:24]
  assign _GEN_12 = _T_18 | _GEN_11; // @[findemax.scala 69:36]
  assign _GEN_14 = _T_17 ? _GEN_12 : _GEN_11; // @[findemax.scala 67:25]
  assign _GEN_17 = rd_en_i ? _GEN_14 : _GEN_11; // @[findemax.scala 65:22]
  assign io_s_port_ready = full_i == 1'h0; // @[findemax.scala 49:21]
  assign io_m_port_valid = empty_i == 1'h0; // @[findemax.scala 50:21]
  assign io_m_port_bits = rd_idx ? data_1 : data_0; // @[findemax.scala 44:20]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  data_0 = _RAND_0[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  data_1 = _RAND_1[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  rd_idx = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  wr_idx = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  full_i = _RAND_4[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {1{`RANDOM}};
  empty_i = _RAND_5[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end
  always @(posedge clock) begin
    if (reset) begin
      data_0 <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (1'h0 == wr_idx) begin
          data_0 <= io_s_port_bits;
        end
      end
    end
    if (reset) begin
      data_1 <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (wr_idx) begin
          data_1 <= io_s_port_bits;
        end
      end
    end
    if (reset) begin
      rd_idx <= 1'h0;
    end else begin
      if (rd_en_i) begin
        rd_idx <= _T_13;
      end
    end
    if (reset) begin
      wr_idx <= 1'h0;
    end else begin
      if (wr_en_i) begin
        wr_idx <= _T_9;
      end
    end
    if (reset) begin
      full_i <= 1'h0;
    end else begin
      if (rd_en_i) begin
        if (_T_17) begin
          full_i <= 1'h0;
        end else begin
          if (wr_en_i) begin
            if (_T_15) begin
              full_i <= _GEN_4;
            end
          end
        end
      end else begin
        if (wr_en_i) begin
          if (_T_15) begin
            full_i <= _GEN_4;
          end
        end
      end
    end
    empty_i <= reset | _GEN_17;
  end
endmodule
module SFIFOCC(
  input         clock,
  input         reset,
  output        io_s_port_ready,
  input         io_s_port_valid,
  input         io_s_port_bits_sign,
  input  [10:0] io_s_port_bits_expo,
  input  [51:0] io_s_port_bits_frac,
  input         io_m_port_ready,
  output        io_m_port_valid,
  output        io_m_port_bits_sign,
  output [10:0] io_m_port_bits_expo,
  output [51:0] io_m_port_bits_frac
);
  reg  data_0_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_0;
  reg [10:0] data_0_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_1;
  reg [51:0] data_0_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_2;
  reg  data_1_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_3;
  reg [10:0] data_1_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_4;
  reg [51:0] data_1_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_5;
  reg  data_2_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_6;
  reg [10:0] data_2_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_7;
  reg [51:0] data_2_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_8;
  reg  data_3_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_9;
  reg [10:0] data_3_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_10;
  reg [51:0] data_3_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_11;
  reg  data_4_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_12;
  reg [10:0] data_4_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_13;
  reg [51:0] data_4_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_14;
  reg  data_5_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_15;
  reg [10:0] data_5_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_16;
  reg [51:0] data_5_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_17;
  reg  data_6_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_18;
  reg [10:0] data_6_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_19;
  reg [51:0] data_6_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_20;
  reg  data_7_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_21;
  reg [10:0] data_7_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_22;
  reg [51:0] data_7_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_23;
  reg  data_8_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_24;
  reg [10:0] data_8_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_25;
  reg [51:0] data_8_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_26;
  reg  data_9_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_27;
  reg [10:0] data_9_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_28;
  reg [51:0] data_9_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_29;
  reg  data_10_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_30;
  reg [10:0] data_10_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_31;
  reg [51:0] data_10_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_32;
  reg  data_11_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_33;
  reg [10:0] data_11_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_34;
  reg [51:0] data_11_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_35;
  reg  data_12_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_36;
  reg [10:0] data_12_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_37;
  reg [51:0] data_12_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_38;
  reg  data_13_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_39;
  reg [10:0] data_13_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_40;
  reg [51:0] data_13_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_41;
  reg  data_14_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_42;
  reg [10:0] data_14_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_43;
  reg [51:0] data_14_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_44;
  reg  data_15_sign; // @[findemax.scala 84:21]
  reg [31:0] _RAND_45;
  reg [10:0] data_15_expo; // @[findemax.scala 84:21]
  reg [31:0] _RAND_46;
  reg [51:0] data_15_frac; // @[findemax.scala 84:21]
  reg [63:0] _RAND_47;
  reg [15:0] rd_idx; // @[findemax.scala 86:23]
  reg [31:0] _RAND_48;
  reg [15:0] wr_idx; // @[findemax.scala 87:23]
  reg [31:0] _RAND_49;
  reg  full_i; // @[findemax.scala 91:23]
  reg [31:0] _RAND_50;
  reg  empty_i; // @[findemax.scala 92:24]
  reg [31:0] _RAND_51;
  wire [3:0] _T_81;
  wire  _GEN_3; // @[findemax.scala 98:20]
  wire [10:0] _GEN_4; // @[findemax.scala 98:20]
  wire [51:0] _GEN_5; // @[findemax.scala 98:20]
  wire  _GEN_6; // @[findemax.scala 98:20]
  wire [10:0] _GEN_7; // @[findemax.scala 98:20]
  wire [51:0] _GEN_8; // @[findemax.scala 98:20]
  wire  _GEN_9; // @[findemax.scala 98:20]
  wire [10:0] _GEN_10; // @[findemax.scala 98:20]
  wire [51:0] _GEN_11; // @[findemax.scala 98:20]
  wire  _GEN_12; // @[findemax.scala 98:20]
  wire [10:0] _GEN_13; // @[findemax.scala 98:20]
  wire [51:0] _GEN_14; // @[findemax.scala 98:20]
  wire  _GEN_15; // @[findemax.scala 98:20]
  wire [10:0] _GEN_16; // @[findemax.scala 98:20]
  wire [51:0] _GEN_17; // @[findemax.scala 98:20]
  wire  _GEN_18; // @[findemax.scala 98:20]
  wire [10:0] _GEN_19; // @[findemax.scala 98:20]
  wire [51:0] _GEN_20; // @[findemax.scala 98:20]
  wire  _GEN_21; // @[findemax.scala 98:20]
  wire [10:0] _GEN_22; // @[findemax.scala 98:20]
  wire [51:0] _GEN_23; // @[findemax.scala 98:20]
  wire  _GEN_24; // @[findemax.scala 98:20]
  wire [10:0] _GEN_25; // @[findemax.scala 98:20]
  wire [51:0] _GEN_26; // @[findemax.scala 98:20]
  wire  _GEN_27; // @[findemax.scala 98:20]
  wire [10:0] _GEN_28; // @[findemax.scala 98:20]
  wire [51:0] _GEN_29; // @[findemax.scala 98:20]
  wire  _GEN_30; // @[findemax.scala 98:20]
  wire [10:0] _GEN_31; // @[findemax.scala 98:20]
  wire [51:0] _GEN_32; // @[findemax.scala 98:20]
  wire  _GEN_33; // @[findemax.scala 98:20]
  wire [10:0] _GEN_34; // @[findemax.scala 98:20]
  wire [51:0] _GEN_35; // @[findemax.scala 98:20]
  wire  _GEN_36; // @[findemax.scala 98:20]
  wire [10:0] _GEN_37; // @[findemax.scala 98:20]
  wire [51:0] _GEN_38; // @[findemax.scala 98:20]
  wire  _GEN_39; // @[findemax.scala 98:20]
  wire [10:0] _GEN_40; // @[findemax.scala 98:20]
  wire [51:0] _GEN_41; // @[findemax.scala 98:20]
  wire  _GEN_42; // @[findemax.scala 98:20]
  wire [10:0] _GEN_43; // @[findemax.scala 98:20]
  wire [51:0] _GEN_44; // @[findemax.scala 98:20]
  wire  _T_82; // @[findemax.scala 100:35]
  wire  wr_en_i; // @[findemax.scala 100:32]
  wire  _T_84; // @[findemax.scala 101:35]
  wire  rd_en_i; // @[findemax.scala 101:32]
  wire [15:0] _T_89; // @[findemax.scala 107:35]
  wire [15:0] _GEN_0; // @[findemax.scala 107:42]
  wire [4:0] _T_90; // @[findemax.scala 107:42]
  wire [15:0] _T_93; // @[findemax.scala 108:35]
  wire [15:0] _GEN_1; // @[findemax.scala 108:42]
  wire [4:0] _T_94; // @[findemax.scala 108:42]
  wire [3:0] _T_96;
  wire  _T_97; // @[findemax.scala 112:13]
  wire [15:0] _GEN_156; // @[findemax.scala 113:22]
  wire  _T_98; // @[findemax.scala 113:22]
  wire  _GEN_96; // @[findemax.scala 113:34]
  wire  _GEN_98; // @[findemax.scala 112:23]
  wire  _GEN_149; // @[findemax.scala 109:20]
  wire  _T_99; // @[findemax.scala 121:13]
  wire [15:0] _GEN_157; // @[findemax.scala 123:22]
  wire  _T_100; // @[findemax.scala 123:22]
  wire  _GEN_150; // @[findemax.scala 123:34]
  wire  _GEN_152; // @[findemax.scala 121:23]
  wire  _GEN_155; // @[findemax.scala 119:20]
  assign _T_81 = rd_idx[3:0];
  assign _GEN_3 = 4'h1 == _T_81 ? data_1_sign : data_0_sign; // @[findemax.scala 98:20]
  assign _GEN_4 = 4'h1 == _T_81 ? data_1_expo : data_0_expo; // @[findemax.scala 98:20]
  assign _GEN_5 = 4'h1 == _T_81 ? data_1_frac : data_0_frac; // @[findemax.scala 98:20]
  assign _GEN_6 = 4'h2 == _T_81 ? data_2_sign : _GEN_3; // @[findemax.scala 98:20]
  assign _GEN_7 = 4'h2 == _T_81 ? data_2_expo : _GEN_4; // @[findemax.scala 98:20]
  assign _GEN_8 = 4'h2 == _T_81 ? data_2_frac : _GEN_5; // @[findemax.scala 98:20]
  assign _GEN_9 = 4'h3 == _T_81 ? data_3_sign : _GEN_6; // @[findemax.scala 98:20]
  assign _GEN_10 = 4'h3 == _T_81 ? data_3_expo : _GEN_7; // @[findemax.scala 98:20]
  assign _GEN_11 = 4'h3 == _T_81 ? data_3_frac : _GEN_8; // @[findemax.scala 98:20]
  assign _GEN_12 = 4'h4 == _T_81 ? data_4_sign : _GEN_9; // @[findemax.scala 98:20]
  assign _GEN_13 = 4'h4 == _T_81 ? data_4_expo : _GEN_10; // @[findemax.scala 98:20]
  assign _GEN_14 = 4'h4 == _T_81 ? data_4_frac : _GEN_11; // @[findemax.scala 98:20]
  assign _GEN_15 = 4'h5 == _T_81 ? data_5_sign : _GEN_12; // @[findemax.scala 98:20]
  assign _GEN_16 = 4'h5 == _T_81 ? data_5_expo : _GEN_13; // @[findemax.scala 98:20]
  assign _GEN_17 = 4'h5 == _T_81 ? data_5_frac : _GEN_14; // @[findemax.scala 98:20]
  assign _GEN_18 = 4'h6 == _T_81 ? data_6_sign : _GEN_15; // @[findemax.scala 98:20]
  assign _GEN_19 = 4'h6 == _T_81 ? data_6_expo : _GEN_16; // @[findemax.scala 98:20]
  assign _GEN_20 = 4'h6 == _T_81 ? data_6_frac : _GEN_17; // @[findemax.scala 98:20]
  assign _GEN_21 = 4'h7 == _T_81 ? data_7_sign : _GEN_18; // @[findemax.scala 98:20]
  assign _GEN_22 = 4'h7 == _T_81 ? data_7_expo : _GEN_19; // @[findemax.scala 98:20]
  assign _GEN_23 = 4'h7 == _T_81 ? data_7_frac : _GEN_20; // @[findemax.scala 98:20]
  assign _GEN_24 = 4'h8 == _T_81 ? data_8_sign : _GEN_21; // @[findemax.scala 98:20]
  assign _GEN_25 = 4'h8 == _T_81 ? data_8_expo : _GEN_22; // @[findemax.scala 98:20]
  assign _GEN_26 = 4'h8 == _T_81 ? data_8_frac : _GEN_23; // @[findemax.scala 98:20]
  assign _GEN_27 = 4'h9 == _T_81 ? data_9_sign : _GEN_24; // @[findemax.scala 98:20]
  assign _GEN_28 = 4'h9 == _T_81 ? data_9_expo : _GEN_25; // @[findemax.scala 98:20]
  assign _GEN_29 = 4'h9 == _T_81 ? data_9_frac : _GEN_26; // @[findemax.scala 98:20]
  assign _GEN_30 = 4'ha == _T_81 ? data_10_sign : _GEN_27; // @[findemax.scala 98:20]
  assign _GEN_31 = 4'ha == _T_81 ? data_10_expo : _GEN_28; // @[findemax.scala 98:20]
  assign _GEN_32 = 4'ha == _T_81 ? data_10_frac : _GEN_29; // @[findemax.scala 98:20]
  assign _GEN_33 = 4'hb == _T_81 ? data_11_sign : _GEN_30; // @[findemax.scala 98:20]
  assign _GEN_34 = 4'hb == _T_81 ? data_11_expo : _GEN_31; // @[findemax.scala 98:20]
  assign _GEN_35 = 4'hb == _T_81 ? data_11_frac : _GEN_32; // @[findemax.scala 98:20]
  assign _GEN_36 = 4'hc == _T_81 ? data_12_sign : _GEN_33; // @[findemax.scala 98:20]
  assign _GEN_37 = 4'hc == _T_81 ? data_12_expo : _GEN_34; // @[findemax.scala 98:20]
  assign _GEN_38 = 4'hc == _T_81 ? data_12_frac : _GEN_35; // @[findemax.scala 98:20]
  assign _GEN_39 = 4'hd == _T_81 ? data_13_sign : _GEN_36; // @[findemax.scala 98:20]
  assign _GEN_40 = 4'hd == _T_81 ? data_13_expo : _GEN_37; // @[findemax.scala 98:20]
  assign _GEN_41 = 4'hd == _T_81 ? data_13_frac : _GEN_38; // @[findemax.scala 98:20]
  assign _GEN_42 = 4'he == _T_81 ? data_14_sign : _GEN_39; // @[findemax.scala 98:20]
  assign _GEN_43 = 4'he == _T_81 ? data_14_expo : _GEN_40; // @[findemax.scala 98:20]
  assign _GEN_44 = 4'he == _T_81 ? data_14_frac : _GEN_41; // @[findemax.scala 98:20]
  assign _T_82 = full_i == 1'h0; // @[findemax.scala 100:35]
  assign wr_en_i = io_s_port_valid & _T_82; // @[findemax.scala 100:32]
  assign _T_84 = empty_i == 1'h0; // @[findemax.scala 101:35]
  assign rd_en_i = io_m_port_ready & _T_84; // @[findemax.scala 101:32]
  assign _T_89 = wr_idx + 16'h1; // @[findemax.scala 107:35]
  assign _GEN_0 = _T_89 % 16'h10; // @[findemax.scala 107:42]
  assign _T_90 = _GEN_0[4:0]; // @[findemax.scala 107:42]
  assign _T_93 = rd_idx + 16'h1; // @[findemax.scala 108:35]
  assign _GEN_1 = _T_93 % 16'h10; // @[findemax.scala 108:42]
  assign _T_94 = _GEN_1[4:0]; // @[findemax.scala 108:42]
  assign _T_96 = wr_idx[3:0];
  assign _T_97 = rd_en_i == 1'h0; // @[findemax.scala 112:13]
  assign _GEN_156 = {{11'd0}, _T_90}; // @[findemax.scala 113:22]
  assign _T_98 = _GEN_156 == rd_idx; // @[findemax.scala 113:22]
  assign _GEN_96 = _T_98 | full_i; // @[findemax.scala 113:34]
  assign _GEN_98 = _T_97 ? 1'h0 : empty_i; // @[findemax.scala 112:23]
  assign _GEN_149 = wr_en_i ? _GEN_98 : empty_i; // @[findemax.scala 109:20]
  assign _T_99 = wr_en_i == 1'h0; // @[findemax.scala 121:13]
  assign _GEN_157 = {{11'd0}, _T_94}; // @[findemax.scala 123:22]
  assign _T_100 = _GEN_157 == wr_idx; // @[findemax.scala 123:22]
  assign _GEN_150 = _T_100 | _GEN_149; // @[findemax.scala 123:34]
  assign _GEN_152 = _T_99 ? _GEN_150 : _GEN_149; // @[findemax.scala 121:23]
  assign _GEN_155 = rd_en_i ? _GEN_152 : _GEN_149; // @[findemax.scala 119:20]
  assign io_s_port_ready = full_i == 1'h0; // @[findemax.scala 103:21]
  assign io_m_port_valid = empty_i == 1'h0; // @[findemax.scala 104:21]
  assign io_m_port_bits_sign = 4'hf == _T_81 ? data_15_sign : _GEN_42; // @[findemax.scala 98:20]
  assign io_m_port_bits_expo = 4'hf == _T_81 ? data_15_expo : _GEN_43; // @[findemax.scala 98:20]
  assign io_m_port_bits_frac = 4'hf == _T_81 ? data_15_frac : _GEN_44; // @[findemax.scala 98:20]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  data_0_sign = _RAND_0[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  data_0_expo = _RAND_1[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {2{`RANDOM}};
  data_0_frac = _RAND_2[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_3 = {1{`RANDOM}};
  data_1_sign = _RAND_3[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_4 = {1{`RANDOM}};
  data_1_expo = _RAND_4[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_5 = {2{`RANDOM}};
  data_1_frac = _RAND_5[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_6 = {1{`RANDOM}};
  data_2_sign = _RAND_6[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_7 = {1{`RANDOM}};
  data_2_expo = _RAND_7[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_8 = {2{`RANDOM}};
  data_2_frac = _RAND_8[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_9 = {1{`RANDOM}};
  data_3_sign = _RAND_9[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_10 = {1{`RANDOM}};
  data_3_expo = _RAND_10[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_11 = {2{`RANDOM}};
  data_3_frac = _RAND_11[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_12 = {1{`RANDOM}};
  data_4_sign = _RAND_12[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_13 = {1{`RANDOM}};
  data_4_expo = _RAND_13[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_14 = {2{`RANDOM}};
  data_4_frac = _RAND_14[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_15 = {1{`RANDOM}};
  data_5_sign = _RAND_15[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_16 = {1{`RANDOM}};
  data_5_expo = _RAND_16[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_17 = {2{`RANDOM}};
  data_5_frac = _RAND_17[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_18 = {1{`RANDOM}};
  data_6_sign = _RAND_18[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_19 = {1{`RANDOM}};
  data_6_expo = _RAND_19[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_20 = {2{`RANDOM}};
  data_6_frac = _RAND_20[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_21 = {1{`RANDOM}};
  data_7_sign = _RAND_21[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_22 = {1{`RANDOM}};
  data_7_expo = _RAND_22[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_23 = {2{`RANDOM}};
  data_7_frac = _RAND_23[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_24 = {1{`RANDOM}};
  data_8_sign = _RAND_24[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_25 = {1{`RANDOM}};
  data_8_expo = _RAND_25[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_26 = {2{`RANDOM}};
  data_8_frac = _RAND_26[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_27 = {1{`RANDOM}};
  data_9_sign = _RAND_27[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_28 = {1{`RANDOM}};
  data_9_expo = _RAND_28[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_29 = {2{`RANDOM}};
  data_9_frac = _RAND_29[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_30 = {1{`RANDOM}};
  data_10_sign = _RAND_30[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_31 = {1{`RANDOM}};
  data_10_expo = _RAND_31[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_32 = {2{`RANDOM}};
  data_10_frac = _RAND_32[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_33 = {1{`RANDOM}};
  data_11_sign = _RAND_33[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_34 = {1{`RANDOM}};
  data_11_expo = _RAND_34[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_35 = {2{`RANDOM}};
  data_11_frac = _RAND_35[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_36 = {1{`RANDOM}};
  data_12_sign = _RAND_36[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_37 = {1{`RANDOM}};
  data_12_expo = _RAND_37[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_38 = {2{`RANDOM}};
  data_12_frac = _RAND_38[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_39 = {1{`RANDOM}};
  data_13_sign = _RAND_39[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_40 = {1{`RANDOM}};
  data_13_expo = _RAND_40[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_41 = {2{`RANDOM}};
  data_13_frac = _RAND_41[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_42 = {1{`RANDOM}};
  data_14_sign = _RAND_42[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_43 = {1{`RANDOM}};
  data_14_expo = _RAND_43[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_44 = {2{`RANDOM}};
  data_14_frac = _RAND_44[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_45 = {1{`RANDOM}};
  data_15_sign = _RAND_45[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_46 = {1{`RANDOM}};
  data_15_expo = _RAND_46[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_47 = {2{`RANDOM}};
  data_15_frac = _RAND_47[51:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_48 = {1{`RANDOM}};
  rd_idx = _RAND_48[15:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_49 = {1{`RANDOM}};
  wr_idx = _RAND_49[15:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_50 = {1{`RANDOM}};
  full_i = _RAND_50[0:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_51 = {1{`RANDOM}};
  empty_i = _RAND_51[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end
  always @(posedge clock) begin
    if (reset) begin
      data_0_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h0 == _T_96) begin
          data_0_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_0_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h0 == _T_96) begin
          data_0_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_0_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h0 == _T_96) begin
          data_0_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_1_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h1 == _T_96) begin
          data_1_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_1_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h1 == _T_96) begin
          data_1_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_1_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h1 == _T_96) begin
          data_1_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_2_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h2 == _T_96) begin
          data_2_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_2_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h2 == _T_96) begin
          data_2_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_2_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h2 == _T_96) begin
          data_2_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_3_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h3 == _T_96) begin
          data_3_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_3_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h3 == _T_96) begin
          data_3_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_3_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h3 == _T_96) begin
          data_3_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_4_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h4 == _T_96) begin
          data_4_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_4_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h4 == _T_96) begin
          data_4_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_4_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h4 == _T_96) begin
          data_4_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_5_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h5 == _T_96) begin
          data_5_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_5_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h5 == _T_96) begin
          data_5_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_5_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h5 == _T_96) begin
          data_5_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_6_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h6 == _T_96) begin
          data_6_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_6_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h6 == _T_96) begin
          data_6_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_6_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h6 == _T_96) begin
          data_6_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_7_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h7 == _T_96) begin
          data_7_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_7_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h7 == _T_96) begin
          data_7_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_7_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h7 == _T_96) begin
          data_7_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_8_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h8 == _T_96) begin
          data_8_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_8_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h8 == _T_96) begin
          data_8_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_8_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h8 == _T_96) begin
          data_8_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_9_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h9 == _T_96) begin
          data_9_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_9_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h9 == _T_96) begin
          data_9_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_9_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'h9 == _T_96) begin
          data_9_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_10_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'ha == _T_96) begin
          data_10_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_10_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'ha == _T_96) begin
          data_10_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_10_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'ha == _T_96) begin
          data_10_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_11_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hb == _T_96) begin
          data_11_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_11_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hb == _T_96) begin
          data_11_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_11_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hb == _T_96) begin
          data_11_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_12_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hc == _T_96) begin
          data_12_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_12_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hc == _T_96) begin
          data_12_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_12_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hc == _T_96) begin
          data_12_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_13_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hd == _T_96) begin
          data_13_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_13_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hd == _T_96) begin
          data_13_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_13_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hd == _T_96) begin
          data_13_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_14_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'he == _T_96) begin
          data_14_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_14_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'he == _T_96) begin
          data_14_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_14_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'he == _T_96) begin
          data_14_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      data_15_sign <= 1'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hf == _T_96) begin
          data_15_sign <= io_s_port_bits_sign;
        end
      end
    end
    if (reset) begin
      data_15_expo <= 11'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hf == _T_96) begin
          data_15_expo <= io_s_port_bits_expo;
        end
      end
    end
    if (reset) begin
      data_15_frac <= 52'h0;
    end else begin
      if (wr_en_i) begin
        if (4'hf == _T_96) begin
          data_15_frac <= io_s_port_bits_frac;
        end
      end
    end
    if (reset) begin
      rd_idx <= 16'h0;
    end else begin
      if (rd_en_i) begin
        rd_idx <= {{11'd0}, _T_94};
      end
    end
    if (reset) begin
      wr_idx <= 16'h0;
    end else begin
      if (wr_en_i) begin
        wr_idx <= {{11'd0}, _T_90};
      end
    end
    if (reset) begin
      full_i <= 1'h0;
    end else begin
      if (rd_en_i) begin
        if (_T_99) begin
          full_i <= 1'h0;
        end else begin
          if (wr_en_i) begin
            if (_T_97) begin
              full_i <= _GEN_96;
            end
          end
        end
      end else begin
        if (wr_en_i) begin
          if (_T_97) begin
            full_i <= _GEN_96;
          end
        end
      end
    end
    empty_i <= reset | _GEN_155;
  end
endmodule
module Findemax(
  input         clock,
  input         reset,
  output        io_s_fp_ready,
  input         io_s_fp_valid,
  input         io_s_fp_bits_sign,
  input  [10:0] io_s_fp_bits_expo,
  input  [51:0] io_s_fp_bits_frac,
  input         io_m_fp_ready,
  output        io_m_fp_valid,
  output        io_m_fp_bits_sign,
  output [10:0] io_m_fp_bits_expo,
  output [51:0] io_m_fp_bits_frac,
  input         io_m_ex_ready,
  output        io_m_ex_valid,
  output [10:0] io_m_ex_bits
);
  wire  u_reg_ex_clock; // @[findemax.scala 159:24]
  wire  u_reg_ex_reset; // @[findemax.scala 159:24]
  wire  u_reg_ex_io_s_port_ready; // @[findemax.scala 159:24]
  wire  u_reg_ex_io_s_port_valid; // @[findemax.scala 159:24]
  wire [10:0] u_reg_ex_io_s_port_bits; // @[findemax.scala 159:24]
  wire  u_reg_ex_io_m_port_ready; // @[findemax.scala 159:24]
  wire  u_reg_ex_io_m_port_valid; // @[findemax.scala 159:24]
  wire [10:0] u_reg_ex_io_m_port_bits; // @[findemax.scala 159:24]
  wire  u_que_fp_clock; // @[findemax.scala 160:24]
  wire  u_que_fp_reset; // @[findemax.scala 160:24]
  wire  u_que_fp_io_s_port_ready; // @[findemax.scala 160:24]
  wire  u_que_fp_io_s_port_valid; // @[findemax.scala 160:24]
  wire  u_que_fp_io_s_port_bits_sign; // @[findemax.scala 160:24]
  wire [10:0] u_que_fp_io_s_port_bits_expo; // @[findemax.scala 160:24]
  wire [51:0] u_que_fp_io_s_port_bits_frac; // @[findemax.scala 160:24]
  wire  u_que_fp_io_m_port_ready; // @[findemax.scala 160:24]
  wire  u_que_fp_io_m_port_valid; // @[findemax.scala 160:24]
  wire  u_que_fp_io_m_port_bits_sign; // @[findemax.scala 160:24]
  wire [10:0] u_que_fp_io_m_port_bits_expo; // @[findemax.scala 160:24]
  wire [51:0] u_que_fp_io_m_port_bits_frac; // @[findemax.scala 160:24]
  reg [3:0] count; // @[findemax.scala 144:22]
  reg [31:0] _RAND_0;
  reg [10:0] emax; // @[findemax.scala 145:21]
  reg [31:0] _RAND_1;
  reg  emax_v; // @[findemax.scala 146:23]
  reg [31:0] _RAND_2;
  wire  c_fp_ready; // @[findemax.scala 151:18 findemax.scala 165:22]
  wire  _T_4; // @[findemax.scala 180:29]
  wire  _T_5; // @[findemax.scala 180:77]
  wire  _T_6; // @[findemax.scala 180:85]
  wire  c_sync; // @[findemax.scala 180:73]
  wire  _T_8; // @[findemax.scala 184:22]
  wire [3:0] _T_10; // @[findemax.scala 188:35]
  wire [4:0] _GEN_0; // @[findemax.scala 187:18]
  wire [4:0] _GEN_1; // @[findemax.scala 186:18]
  wire  _T_11; // @[findemax.scala 190:17]
  wire  _T_12; // @[findemax.scala 192:41]
  wire  _T_13; // @[findemax.scala 192:30]
  wire  _T_15; // @[findemax.scala 197:22]
  wire  _GEN_5; // @[findemax.scala 197:31]
  wire [3:0] _GEN_7; // @[findemax.scala 187:26 findemax.scala 188:26]
  wire [4:0] _GEN_8; // @[findemax.scala 187:26 findemax.scala 188:26]
  SRegFWREV u_reg_ex ( // @[findemax.scala 159:24]
    .clock(u_reg_ex_clock),
    .reset(u_reg_ex_reset),
    .io_s_port_ready(u_reg_ex_io_s_port_ready),
    .io_s_port_valid(u_reg_ex_io_s_port_valid),
    .io_s_port_bits(u_reg_ex_io_s_port_bits),
    .io_m_port_ready(u_reg_ex_io_m_port_ready),
    .io_m_port_valid(u_reg_ex_io_m_port_valid),
    .io_m_port_bits(u_reg_ex_io_m_port_bits)
  );
  SFIFOCC u_que_fp ( // @[findemax.scala 160:24]
    .clock(u_que_fp_clock),
    .reset(u_que_fp_reset),
    .io_s_port_ready(u_que_fp_io_s_port_ready),
    .io_s_port_valid(u_que_fp_io_s_port_valid),
    .io_s_port_bits_sign(u_que_fp_io_s_port_bits_sign),
    .io_s_port_bits_expo(u_que_fp_io_s_port_bits_expo),
    .io_s_port_bits_frac(u_que_fp_io_s_port_bits_frac),
    .io_m_port_ready(u_que_fp_io_m_port_ready),
    .io_m_port_valid(u_que_fp_io_m_port_valid),
    .io_m_port_bits_sign(u_que_fp_io_m_port_bits_sign),
    .io_m_port_bits_expo(u_que_fp_io_m_port_bits_expo),
    .io_m_port_bits_frac(u_que_fp_io_m_port_bits_frac)
  );
  assign c_fp_ready = u_que_fp_io_s_port_ready; // @[findemax.scala 151:18 findemax.scala 165:22]
  assign _T_4 = io_s_fp_valid & c_fp_ready; // @[findemax.scala 180:29]
  assign _T_5 = emax_v == 1'h0; // @[findemax.scala 180:77]
  assign _T_6 = _T_5 | u_reg_ex_io_s_port_ready; // @[findemax.scala 180:85]
  assign c_sync = _T_4 & _T_6; // @[findemax.scala 180:73]
  assign _T_8 = count == 4'h0; // @[findemax.scala 184:22]
  assign _T_10 = count - 4'h1; // @[findemax.scala 188:35]
  assign _GEN_0 = _T_8 ? 5'h10 : {{1'd0}, _T_10}; // @[findemax.scala 187:18]
  assign _GEN_1 = c_sync ? _GEN_0 : {{1'd0}, count}; // @[findemax.scala 186:18]
  assign _T_11 = emax_v & u_reg_ex_io_s_port_ready; // @[findemax.scala 190:17]
  assign _T_12 = io_s_fp_bits_expo > emax; // @[findemax.scala 192:41]
  assign _T_13 = io_s_fp_valid & _T_12; // @[findemax.scala 192:30]
  assign _T_15 = c_sync & _T_8; // @[findemax.scala 197:22]
  assign _GEN_5 = _T_15 | emax_v; // @[findemax.scala 197:31]
  assign io_s_fp_ready = _T_4 & _T_6; // @[findemax.scala 164:11 findemax.scala 172:19]
  assign io_m_fp_valid = u_que_fp_io_m_port_valid; // @[findemax.scala 166:11]
  assign io_m_fp_bits_sign = u_que_fp_io_m_port_bits_sign; // @[findemax.scala 166:11]
  assign io_m_fp_bits_expo = u_que_fp_io_m_port_bits_expo; // @[findemax.scala 166:11]
  assign io_m_fp_bits_frac = u_que_fp_io_m_port_bits_frac; // @[findemax.scala 166:11]
  assign io_m_ex_valid = u_reg_ex_io_m_port_valid; // @[findemax.scala 162:11]
  assign io_m_ex_bits = u_reg_ex_io_m_port_bits; // @[findemax.scala 162:11]
  assign _GEN_7 = _GEN_1[3:0]; // @[findemax.scala 187:26 findemax.scala 188:26]
  assign _GEN_8 = reset ? 5'h10 : {{1'd0}, _GEN_7}; // @[findemax.scala 187:26 findemax.scala 188:26]
  assign u_reg_ex_clock = clock;
  assign u_reg_ex_reset = reset;
  assign u_reg_ex_io_s_port_valid = emax_v; // @[findemax.scala 163:22]
  assign u_reg_ex_io_s_port_bits = emax; // @[findemax.scala 163:22]
  assign u_reg_ex_io_m_port_ready = io_m_ex_ready; // @[findemax.scala 162:11]
  assign u_que_fp_clock = clock;
  assign u_que_fp_reset = reset;
  assign u_que_fp_io_s_port_valid = _T_4 & _T_6; // @[findemax.scala 164:11 findemax.scala 165:22]
  assign u_que_fp_io_s_port_bits_sign = io_s_fp_bits_sign; // @[findemax.scala 164:11 findemax.scala 165:22]
  assign u_que_fp_io_s_port_bits_expo = io_s_fp_bits_expo; // @[findemax.scala 164:11 findemax.scala 165:22]
  assign u_que_fp_io_s_port_bits_frac = io_s_fp_bits_frac; // @[findemax.scala 164:11 findemax.scala 165:22]
  assign u_que_fp_io_m_port_ready = io_m_fp_ready; // @[findemax.scala 166:11]
`ifdef RANDOMIZE_GARBAGE_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_INVALID_ASSIGN
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_REG_INIT
`define RANDOMIZE
`endif
`ifdef RANDOMIZE_MEM_INIT
`define RANDOMIZE
`endif
`ifndef RANDOM
`define RANDOM $random
`endif
`ifdef RANDOMIZE_MEM_INIT
  integer initvar;
`endif
initial begin
  `ifdef RANDOMIZE
    `ifdef INIT_RANDOM
      `INIT_RANDOM
    `endif
    `ifndef VERILATOR
      `ifdef RANDOMIZE_DELAY
        #`RANDOMIZE_DELAY begin end
      `else
        #0.002 begin end
      `endif
    `endif
  `ifdef RANDOMIZE_REG_INIT
  _RAND_0 = {1{`RANDOM}};
  count = _RAND_0[3:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_1 = {1{`RANDOM}};
  emax = _RAND_1[10:0];
  `endif // RANDOMIZE_REG_INIT
  `ifdef RANDOMIZE_REG_INIT
  _RAND_2 = {1{`RANDOM}};
  emax_v = _RAND_2[0:0];
  `endif // RANDOMIZE_REG_INIT
  `endif // RANDOMIZE
end
  always @(posedge clock) begin
    count <= _GEN_8[3:0];
    if (reset) begin
      emax <= 11'h0;
    end else begin
      if (_T_11) begin
        if (io_s_fp_valid) begin
          emax <= io_s_fp_bits_expo;
        end else begin
          emax <= 11'h0;
        end
      end else begin
        if (_T_13) begin
          emax <= io_s_fp_bits_expo;
        end
      end
    end
    if (reset) begin
      emax_v <= 1'h0;
    end else begin
      if (_T_11) begin
        emax_v <= 1'h0;
      end else begin
        emax_v <= _GEN_5;
      end
    end
  end
endmodule
